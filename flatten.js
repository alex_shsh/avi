

function flatten(...props) {

  function flattenReq(items) {
    const flat = []; 
    items.forEach(item => {
      if (Array.isArray(item)) {
        flat.push(...flattenReq(item));
      } else {
        flat.push(item);
      }
    });
    return flat;
  } 
  return flattenReq(props)
}

// Написать метод flatten
console.log(flatten(1, [2, 3], 4, 5, [6, [7]])) // returns [1, 2, 3, 4, 5, 6, 7]
console.log(flatten('a', ['b', 2], 3, null, [[4], ['c']])) // returns ['a', 'b', 2, 3, null, 4, 'c']
