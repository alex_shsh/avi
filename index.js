const store = {
  button: document.querySelector('.count'), 
  userInput: document.getElementById('userInput'),
  resultInput: document.getElementById('result'),
  selects: {
    from: document.getElementById('from-currency'),
    to: document.getElementById('to-currency'),
  },
  currencies: {}
}

/** Все ровно 1 раз в час не получится(кроме един кейса USD придется делать 2 для других), 
 * тк нету доступного списка валют, 
 * который актуален в настоящий момент для банка */
async function getCurrencies() {
  const now = new Date().getTime()
  const lastTime = localStorage.getItem('lastTimeReceipt')
  const passedHaldHoure = lastTime ? now > +lastTime : false

  if(passedHaldHoure){
    const data = await fetch('https://api.exchangeratesapi.io/latest?base=USD')
    store.currencies = await data.json()
  } else {  
    store.button.disabled = true
    disableInterface()
  }
}


async function core() {
  await getCurrencies()
  initialSelects()
  initialCountResult()
}
core()


function initialSelects() { 
  const selects = store.selects
  const currencies = store.currencies['rates'] 
  
  for (const selectKey in selects) {
    for (let key in currencies) { 
      const opt = document.createElement('option');
      opt.value = key;
      opt.innerHTML = key;  
      selects[selectKey].appendChild(opt) 
    }   
  }   
}


function initialCountResult() {  
  store['button'].addEventListener('click', count)
 
  async function count() {
    const fromVal = store.selects['from'].value
    const toVal = store.selects['to'].value   
    const getData = await fetch('https://api.exchangeratesapi.io/latest?base='+toVal)
    const data = await getData.json()

    const one = data['rates'][fromVal]
    const number = store.userInput.value
    const res = (one * number).toFixed(2) 
    store.resultInput.value = res
    localStorage.setItem('lastTimeReceipt', new Date(Date.now() + (30 * 60 * 1000)).getTime())

    // test 15 sec
    // localStorage.setItem('lastTimeReceipt', new Date(Date.now() + (15 * 1000)).getTime())

    disableInterface()
    setTimeout(() => { 
      core()
    }, 30 * 60 * 1000);
  }
}

function disableInterface(){ 
  store.button.disabled = true
  store.userInput.disabled = true
  store.selects.from.disabled = true
  store.selects.to.disabled = true
}